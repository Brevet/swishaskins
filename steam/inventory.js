var request = require('request');
var _ = require('lodash');

 // var url = "http://steamcommunity.com/profiles/76561198007691048/inventory/json/730/2";


module.exports = { getData: function getInventory(url, callback)
{
    // Load inventory from user
    request({
            url: url,
            json: true
        }, function jsonParse(error, response, data) {
            if(!error){
                if(data.length > 10){
                    console.log(error)
                }
                var ids = getID(data.rgInventory);
                var item = getItems(data.rgDescriptions);

                // Link item with ID-sheet to access img url etc.
                for (var i = 0; i < ids.length; i++) {
                    for (var k = 0; k < item.length; k++) {
                        if (ids[i].classid == item[k].classid) {
                            ids[i].market_name = item[k].market_name;
                            ids[i].icon_url = item[k].icon_url;
                            ids[i].tradable = item[k].tradable;
                        }
                    }
                }
            }

            callback(ids);
        }
    );

    // Sort out unimportant information
    function getItems(data) {
        var item = data;
        if (!item) {
            return "error";

        }
        return _(item).keys().map(function (id) {
            return _.pick(item[id], ["market_name", "icon_url", "tradable", "classid"]);
        }).value();
    }

    // Sort IDs
    function getID(data) {
        var item = data;
        if (!item) {
            return "error";

        }
        return _(item).keys().map(function (id) {
            return _.pick(item[id], ["classid", "id"]);
        }).value();


    }
}
};
