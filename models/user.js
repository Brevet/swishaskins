var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Schema
var account = new Schema({
    phone: String,
    tradelink: String,
    steamId: String,
    email: String,
    nameForAndAfter: String,
    steamInfo: []
});

var accountModel = mongoose.model('Account', account);
module.exports = accountModel;


