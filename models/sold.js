var mongoose = require('mongoose');
var Schema = mongoose.Schema;


//Schema
var sold = new Schema({
    sellerId: String,
    buyerId: String,
    icon_url: String,
    wName: String
});

module.exports = mongoose.model('Sold', sold);
