var mongoose = require('mongoose');
var Schema = mongoose.Schema;


//Schema
var skin = new Schema({
    price: Number,
    wName: String,
    classId: Number,
    wType: String,
    wHead: String,
    icon_url: String,
    statrak: Number,
    exterior: String,
    sellerId: String,
    seller: []
});

module.exports = mongoose.model('Skin', skin);
