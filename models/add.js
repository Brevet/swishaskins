var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Schema
var add = new Schema({
    price: Number,
    wName: String,
    classId: Number,
    icon_url: String,
    wType: String,
    statrak: Number,
    exterior: String,
    sellerName: String,
    sellerId: String,
    sellerPhone: Number,
    seller: Array
});


module.exports = mongoose.model('Add', add);
