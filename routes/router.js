//Moduler
var express = require('express');
var router = express.Router();
var passport = require('passport');
SteamStrategy = require('passport-steam').Strategy;
var request = require('request');
var data = require('../steam/inventory.js');
var Account = require('../models/user');
var Skin = require('../models/skin');
var Add = require('../models/add');
var mongoose = require('mongoose');
var onHold = require('../models/onHold');
var Sold = require('../models/sold');


//Första sida
router.get('/', function (req, res){
    var user = req.user;
    if(!user) {
        res.render('index', {'title': 'SwishaSkins', user: req.user})
    }else{
        res.render('loggedIn', {user: req.user})
}});

//Market sida
router.get('/market', function(req, res){
    var user = req.user;
    if(typeof req.query.wHead != "undefined"){
        var q = req.query;
        if(req.query.wName === ''){
            delete q.wName
        }
        if(req.query.wType === ''){
            delete q.wType
        }
        if(req.query.statrak === ''){
            delete q.statrak
        }
        if(req.query.exterior === ''){
            delete q.exterior
        }
        if(req.query.wHead === ''){
            delete q.wHead;
        }
        if(req.query.wHead === "button"){
            delete q.wHead;
            Skin.find(q)
                .exec(function(err, skin){
                    if(err){
                        console.log("Error")
                    }else{
                        if(!user) {
                            res.render('market', {skins: JSON.stringify(skin)})
                        }else{
                            res.render('marketUser', {user: req.user, skins: JSON.stringify(skin)})
                        }
                    }
                })
        }else {
            var high = req.query.high;
            var low = req.query.low;
            var sort = req.query.sortera;
            delete q.sortera;
            delete q.low;
            delete q.high;
            var data;
            var test = 1;
            Skin.find(q)
                .exec(function (err, skin) {
                    if (err) {
                        console.log("error")
                    } else {
                        if (!user) {
                            if (low === '') {
                                if (high === '') {
                                    data = skin;
                                } else {
                                    data = skin.filter(function (pris) {
                                        test = 123;
                                        return pris.price <= high

                                    })
                                }
                            } else {
                                if (high === '') {
                                    data = skin.filter(function (pris) {
                                        test = 123;
                                        return pris.price >= low
                                    })
                                } else {
                                    data = skin.filter(function (pris) {
                                        test = 123;
                                        return pris.price >= low && pris.price <= high
                                    })
                                }
                            }
                            if (sort === '') {

                            } else {
                                if (sort === 'lowHigh') {
                                    data.sort(function (x, y) {
                                        var xpris = x.price;
                                        var ypris = y.price;
                                        return xpris - ypris;
                                    })
                                } else {
                                    if (sort === 'highLow') {
                                        data.sort(function (x, y) {
                                            var xpris = x.price;
                                            var ypris = y.price;
                                            return (ypris - xpris)
                                        })
                                    } else {

                                    }
                                }
                            }
                            if (data.length > 0) {
                                skin = data;
                            }
                            if (test.toString().length > 1 && data.length < 1) {
                                skin = {}
                            }

                            res.render('market', {skins: JSON.stringify(skin)})

                        } else {

                            if (low === '') {
                                if (high === '') {
                                    data = skin;
                                } else {
                                    data = skin.filter(function (pris) {
                                        test = 123;
                                        return pris.price <= high

                                    })
                                }
                            } else {
                                if (high === '') {
                                    data = skin.filter(function (pris) {
                                        test = 123;
                                        return pris.price >= low
                                    })
                                } else {
                                    data = skin.filter(function (pris) {
                                        test = 123;
                                        return pris.price >= low && pris.price <= high
                                    })
                                }
                            }
                            if (sort === '') {

                            } else {
                                if (sort === 'lowHigh') {
                                    data.sort(function (x, y) {
                                        var xpris = x.price;
                                        var ypris = y.price;
                                        return xpris - ypris;
                                    })
                                } else {
                                    if (sort === 'highLow') {
                                        data.sort(function (x, y) {
                                            var xpris = x.price;
                                            var ypris = y.price;
                                            return (ypris - xpris)
                                        })
                                    } else {

                                    }
                                }
                            }
                            if (data.length > 0) {
                                skin = data;
                            }
                            if (test.toString().length > 1 && data.length < 1) {
                                skin = {}
                            }
                            res.render('marketUser', {user: req.user, skins: JSON.stringify(skin)})
                        }
                    }
                })
        }}else{
        Skin.find({})
            .exec(function(err, items){
                if(err){
                    console.log("error")
                }else{
                    if(!user) {
                        res.render('market', {skins: JSON.stringify(items)})
                    }else{
                        res.render('marketUser', {user: req.user, skins: JSON.stringify(items)})
                    }
                }
            })}});

//Profil sida
router.get('/profile', function (req, res){
    var user = req.user;
    if(!user){
        res.redirect('/auth/steam')
    }else {
        res.render('profile', {'title': 'profile', user: req.user})
    }});
router.get('/profile.json', function(req, res){
    Account.find({steamId: req.user.id})
        .exec(function(err, account){
            if(err){
                console.log("Error")
            }else{
                console.log(account);
                res.send(account);
            }
        });

});

router.get('/profile/:id', function(req, res){
   Account.findOne({_id: req.params.id})
       .exec(function(err, acc){
           if(err){
               console.log("error")
           }else{
               res.render('profile', {profile: JSON.stringify(acc)})
           }
       })
});

//Sälj sida
router.get('/sell', function (req, res){
    var user = req.user;
    if(!user){
        res.redirect('/auth/steam');
    }else {
        var id = req.user.id.toString();
        var slice = id.length;
        var id1 = id.slice(0, 10);
        var id2 = id.slice(10, slice);
        res.render('sell', {user: req.user, id1: id1, id2: id2})
    }
});

//Hjälp sida
router.get('/help', function(req, res){
   res.render('Hjälp', {'title': 'Hjälp'})
});

//Support sida
router.get('/support', function(req, res){
    var user = req.user;
    if(!user){
        res.redirect('/auth/steam')
    }else{
    res.render('support', {'title': "Support", user: req.user})
}});

//Om oss sida
router.get('/about', function(req, res){
    var user = req.user;
    if(!user) {
        res.render('about')
    }else{
        res.render('aboutUser', {user: req.user})
    }
});

//Kundvagn sida
router.get('/kundvagn', function(req, res){
    var user = req.user;
    if(!user){
        res.redirect('/auth/steam');
    }else{
        var id = req.user.id.toString();
        Kundvagn.findOne({steamId: id})
            .exec(function(err, vagn){
                if(err){
                    console.log("Error")
                }else{
                    console.log(vagn.items)
                    res.render('kundvagn', {Vagn: JSON.stringify(vagn)})
                }
            });
    }});

//Inviduell Skin sida
router.get('/market/:id', function(req, res){
    var user = req.user;
    var id = req.params.id;
    Skin.findOne({_id: id})
        .exec(function(err, skin){
            if(err){
                console.log("Error")
            }else{
                if(!user) {
                    res.render('inviduellSkin', {Skin: JSON.stringify(skin)})
                }else{
                    res.render('inviduellSkinUser', {user: req.user, Skin: JSON.stringify(skin)})
                }
            }
        })
});


//DATABAS START

//Skins DB
router.get('/skinDB', function(req, res){
    if(typeof req.query != "undefined"){
        var q = req.query;
        if(req.query.name === ''){
            delete q.wName
        }
        if(req.query.wType === ''){
            delete q.wType
        }
        if(req.query.statrak === ''){
            delete q.statrak
        }
        if(req.query.exterior === ''){
            delete q.exterior
        }
        Skin.find(q)
            .exec(function(err, skin){
                if(err){
                    console.log("error")
                }else{
                    res.send(skin)
                }})
    }else{
        Skin.find({})
            .exec(function(err, items){
                if(err){
                    console.log("error")
                }else{
                   res.send(items)
                }
            })}});
router.post('/skinDB', function(req, res){
    var wType;
    var wHead;
    var exterior;
    var statrak;
    var str = req.body.wName;
    var test = req.body.wName.toLowerCase();
    if(test.includes("statrak")){
        statrak = 1;
    }else{
        statrak = 0;
    }

    //"Others"
    if(test.includes("case")){
        wType = req.body.wName;
        wHead = "Case";
    }

    //Rifles
    if(test.includes("ssg")){
        wType = "SSG";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("ak-47")){
        wType = "AK47";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("aug")){
        wType = "AUG";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("awp")){
        wType = "AWP";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("famas")){
        wType = "Famas";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("g3sg1")){
        wType = "G3SG1";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("galil")){
        wType = "Galil";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("m4a1")){
        wType = "M4A1-S";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("m4a4")){
        wType = "M4A4";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("scar-20")){
        wType = "Scar-20";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("sg 553")){
        wType = "SG 553";
        wHead = "Rifle";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }

    //SMG
    if(test.includes("mac")){
        wType = "MAC-10";
        wHead = "SMG";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("mp7")){
        wType = "MP7";
        wHead = "SMG";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("mp9")){
        wType = "MP9";
        wHead = "SMG";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("pp-bizon")){
        wType = "PP-Bizon";
        wHead = "SMG";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("p90")){
        wType = "P90";
        wHead = "SMG";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("ump-45")){
        wType = "UMP-45";
        wHead = "SMG";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }

    //Heavy
    if(test.includes("mag-7")){
        wType = "MAG-7";
        wHead = "Heavy";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("nova")){
        wType = "Nova";
        wHead = "Heavy";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("sawed")){
        wType = "Sawed-Off";
        wHead = "Heavy";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("xm1014")){
        wType = "XM1014";
        wHead = "Heavy";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("m249")){
        wType = "M249";
        wHead = "Heavy";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("negev")){
        wType = "Negev";
        wHead = "Heavy";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }

    //Knife
    if(test.includes("knife")){
        wType = str;
        wHead = "Knife";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("bayonet")){
        wType = str;
        wHead = "Knife";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("daggers")){
        wType = str;
        wHead = "Knife";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("karambit")){
        wType = str;
        wHead = "Knife";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }

    //Pistol
    if(test.includes("five-seven")){
        wType = "Five-Seven";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("usp")){
        wType = "USP-S";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("tec-9")){
        wType = "Tec-9";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("r8")){
        wType = "R8";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("glock")){
        wType = "Glock";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("berettas")){
        wType = "Dual-Berettas";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("desert eagle")){
        wType = "Desert-Eagle";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("cz75")){
        wType = "CZ75";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("p250")){
        wType = "P250";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }
    if(test.includes("p2000")){
        wType = "P2000";
        wHead = "Pistol";
        exterior = str.substring(str.lastIndexOf("(")+1, str.lastIndexOf(")"));
    }

    
    var id = req.user.id.toString();
    var newSkin = new Skin();
    newSkin.seller = req.user;
    newSkin.wName = req.body.wName;
    newSkin.wType = wType;
    newSkin.statrak = statrak;
    newSkin.exterior = exterior;
    newSkin.wHead = wHead;
    newSkin.price = req.body.price;
    newSkin.icon_url = req.body.icon_url;
    newSkin.classId = req.body.classId;
    newSkin.sellerId = id;

    newSkin.save(function(err, skin){
        if(err){
            console.log("error")
        }else{
            res.send(skin)
        }
    })
});

router.get('/skinDB/:id', function(req, res){
    Skin.findOne({
            _id: req.params.id
        })
        .exec(function(err, skin){
            if(err){
                console.log("err")
            }else{
                res.json(skin);
            }
        })
});
router.delete('/skinDB/:id', function(req, res) {
    Skin.findOne({
        _id: req.params.id}, function(err, skin){
            if(err){
                console.log("Error")
            }else{
                skin.remove();
                res.end();
            }
    });

});

router.get('/skinProfile', function(req, res){
    var id = req.user.id.toString();
    Skin.find({})
        .exec(function(err, skin){
            if(err){
                console.log("Error")
            }else{
                var items = [];
                var k = 0;
                for(var i = 0; i < skin.length; i++){
                    if(skin[i].sellerId === id){
                        k = items.length;
                        items[k] = skin[i];
                    }
                }
                res.send(items)
            }
        })
});

//Users DB
router.get('/userDB', function(req, res){

    Account.find({})
        .exec(function(err, user){
            if(err){
                console.log("error")
            }else{
                res.json(user)
            }
        })
});
router.post('/userDB', function(req, res){
    var newUser = new Account();
    newUser.phone = req.body.phone;

    newUser.save(function(err, user){
        if(err){
            console.log(err)
        }else{
            console.log(user);
            res.send(user);
        }
    });
});

router.get('/userDB/:id', function(req, res){
    Account.findOne({
        _id: req.params.id
    })
        .exec(function(err, user){
            if(err){
                console.log("err")
            }else{
                res.json(user);
            }
        })
});
router.put('/userDB/:id', function(req, res) {
    Account.findOneAndUpdate({
            _id: req.params.id
        },
        { $set: { email: "elegiggle" }
        }, {upsert: true}, function(err, user) {
            if (err) {
                res.send('error updating ');
            } else {
                res.send(user);
            }
        });
});
router.delete('/userDB/:id', function(req, res) {
    Account.findOne({_id: req.params.id}, function(err, acc){
        if(err){
            console.log("Error")
        }else{
            acc.remove();
            res.status(204);
            res.end();
        }
    })});

//sellAdd DB
router.get('/sellAdd', function(req, res){
   Add.find({})
       .exec(function(err, add){
           if(err){
               console.log("error")
           }else{
               res.send(add)
           }
       });
});
router.post('/sellAdd', function(req, res){
    var id = req.user.id.toString();
    if(req.body.method != "POST"){
        console.log(id);
        Add.find({sellerId: id})
            .remove().exec();
        res.end()

    }else{
    var newAdd = new Add();
    newAdd.classId = req.body.classid;
    newAdd.wName = req.body.name;
    newAdd.price = req.body.price;
    newAdd.icon_url = req.body.icon;
    newAdd.sellerId = id;
    //newAdd.seller = req.user;

    newAdd.save(function(err, add){
        if(err){
            console.log("error")
        }else{
            res.send(add)
        }
    })}
});
router.delete('/sellAdd/:id', function(req, res){
    Add.findOne({_id: req.params.id}, function(err, add){
        if(err){
            console.log("Error")
        }else{
            add.remove();
            res.status(204);
            res.end();
        }
    })
});

//Acc DB
router.get('/acc', function(req, res){
    var id = req.user.id.toString();
   Account.findOne({steamId: id})
       .exec(function(err, acc){
           if(err){
               console.log("Error")
           }else{
               res.send(acc)
           }
       })
});
router.post('/acc', function(req, res){
    var id  = req.user.id.toString();
    Account.find({steamId: id})
        .exec(function(err, acc){
            if(err){
                console.log("error")
            }else{
                if(acc.length){
                    console.log("Finns redan")
                }else{
                    var newAccount = new Account();
                    newAccount.nameForAndAfter = req.user._json.realname;
                    newAccount.steamId = id;
                    newAccount.email = '';
                    newAccount.phone = '';
                    newAccount.tradelink = '';
                    newAccount.steamInfo = req.user;
                    newAccount.save(function(err, acc){
                        if(err){
                            console.log("Error")
                        }else{
                            res.send(acc)
                        }
                    })
                }
            }
        })
});
router.put('/acc', function(req, res){
   var user = req.user;
    var id = req.user.id.toString()
    Account.findOneAndUpdate({steamId: id}, {$set: {steamInfo: user}}, {upsert: true}, function(err, acc){
        if(err){
            console.log("Error 3")
        }else{
            res.send(acc)
        }
    }) 
});
router.delete('/acc', function(req, res){
   var pass = req.body.podel;
    var id = req.body.steamId;
    if(pass == "areyouready"){
        Account.findOne({steamId: id})
            .exec(function(err, acc){
                if(err){
                    console.log("Error")
                }else{
                    acc.remove();
                    res.status(204);
                    res.end();
                }
            })
    }else{
        console.log("Nej du");
        res.end();
    }
});

//onHold DB
router.get('/onHold', function(req, res){
    var id = req.body.classId;
   onHold.findOne({classId: id})
       .exec(function(err, onHold){
           if(err){
               console.log("Error")
           }else{
               res.send(onHold)
           }
       })
});
router.post('/onHold', function(req, res){
    var id = req.body.classId;
   Skin.findOne({classId: id})
       .exec(function(err, skin){
           if(err){
               console.log("Error")
           }else{
               var newOnHold = new onHold();
               newOnHold.info = skin;
               newOnHold.save(function(err, onHold){
                   if(err){
                       console.log("Error")
                   }else{
                       res.send(onHold)
                   }
               })
           }
       })

});
router.delete('/onHold', function(req, res){
    var id = req.body.classId;
    onHold.findOne({classId: id})
        .exec(function(err, onHold){
            if(err){
                console.log("Error")
            }else{
                onHold.remove();
                res.status(204);
                res.end()

            }
        })
});

//DATABAS SLUT

//APIs START

//Steam API: Ladda inventory
router.get('/inventory', function (req, res) {
    console.log(req.user.id)
    var url = 'http://steamcommunity.com/profiles/' + req.user.id + '/inventory/json/730/2';
    data.getData(url, function (data) {

        res.send(data);


    });
});

//Steam API: Logga in / Logga ut
router.get('/auth/steam',
    passport.authenticate('steam'),
    function(req, res) {
    });
router.get('/auth/steam/return',
    passport.authenticate('steam', { failureRedirect: '/login' }),
    function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
    });
router.get('/logout', function(req, res){
   req.logout();
   res.redirect('http://localhost:3000/')

});


//APIs SLUT

//Error
router.get('*', function (req, res){
    res.render('error', {'title': 'error'})

});

module.exports = router;